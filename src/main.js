import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  template: '<App v-bind:start="loaded"/>',
  components: { App },
  data () {
    return {
      loaded: false
    }
  },
  created () {
    // Emits from Loader.vue
    this.$on('loaded', () => {
      this.loaded = true
    })
  }
})
