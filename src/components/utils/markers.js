export default function Markers () {
  let markers = []
  let coords = document.querySelectorAll('.map-pin')
  let layerMarkers = document.querySelectorAll('.c-map-markers')[0]
  let base = document.querySelectorAll('.map-pin-base')[0]
  let markerBase = document.querySelectorAll('.c-map-pin')[0]
  // let initialOffset = layerMarkers.parentNode.getBoundingClientRect();
  this.make = function () {
    // keep the map translate value (0 at start)
    let offset = layerMarkers.parentNode.getBoundingClientRect()
    let scale = 1
    // First marker
    let baseRect = base.getBoundingClientRect()
    markerBase.style.left = baseRect.left + 200 - offset.left * scale + 'px'
    markerBase.style.top = baseRect.top + 80 - offset.top * scale + 'px'

    for (let i = 0; i < coords.length; i++) {
      let rect = coords[i].getBoundingClientRect()
      let radius = getRandom(75, 120)

      // Work with previous collection
      if (layerMarkers.children.length === coords.length) {
        let marker = layerMarkers.children[i]
        marker.style.left = rect.left - offset.left * scale + 'px'
        marker.style.top = rect.top - offset.top * scale + 'px'
      } else {
        let div = document.createElement('div')
        let divInner = document.createElement('div')
        divInner.setAttribute('class', 'c-marker__inner')
        div.appendChild(divInner)
        div.setAttribute('class', 'c-marker')
        div.style.left = rect.left + 'px'
        div.style.top = rect.top + 'px'
        div.style.width = radius * 2 + 'px'
        div.style.height = radius * 2 + 'px'
        div.style.opacity = 0

        markers.push(div)
        layerMarkers.appendChild(div)
      }
    }
  }
  this.make()
  window.addEventListener('resize', () => {
    this.make()
  })
  return markers
}
function getRandom (min, max) {
  return Math.random() * (max - min) + min
}

// WEBPACK FOOTER //
// ./src/components/utils/markers.js
