import TweenMax from 'gsap'

export default function (scene) {
  if (window.isMobile && scene.contentTimelineMobile) {
    scene.contentTimelineMobile.progress(0).pause().tweenTo('act-0')
  }
  if (!window.isMobile && scene.contentTimeline) {
    scene.contentTimeline.progress(0).pause()
    scene.contentTimeline.tweenTo('act-0')
  }
}

// WEBPACK FOOTER //
// ./src/components/utils/play.js
