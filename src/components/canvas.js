import * as THREE from 'three'
import TweenMax from 'gsap'

export default function Canvas () {
  // scene with mask pattern
  this.scene = new THREE.Scene()
  // HUD scene with background.
  this.sceneHUD = new THREE.Scene()

  // Create the camera and set the viewport to match the screen dimensions.
  this.camera = new THREE.OrthographicCamera(-window.innerWidth / 2, window.innerWidth / 2, window.innerHeight / 2, -window.innerHeight / 2, 0, 30)
  // Same config..
  this.cameraHUD = new THREE.OrthographicCamera(-window.innerWidth / 2, window.innerWidth / 2, window.innerHeight / 2, -window.innerHeight / 2, 0, 30)

  this.renderer = new THREE.WebGLRenderer({ antialias: false, alpha: true })
  this.renderer.autoClear = false
  this.renderer.setPixelRatio(window.devicePixelRatio)
  this.renderer.setSize(window.innerWidth, window.innerHeight)

  this.makeLines()
}
Canvas.prototype.makeMaskMobile = function () {
  let group = new THREE.Group()
  group.name = 'mask-mobile'
  let timeline = new TimelineMax({
    paused: true,
    onComplete: function () {
      // this.pause()
      // this.time(0)
    }
  })
  let width = window.innerWidth
  let height = window.innerHeight / 4

  for (var j = 0; j < 4; j++) {
    let geo = new THREE.PlaneGeometry(width, height, 2, 1)
    let mat = new THREE.MeshBasicMaterial({ color: 0x000000, opacity: 0 })
    let mask = new THREE.Mesh(geo, mat)
    let delay = j * 0.1

    mask.position.set(-width / 2, height * j - height * 2 + height / 2, 0)
    mask.scale.x = 0.000001

    timeline
      .fromTo(mask.scale, 1, {
        x: 0.000001
      }, {
        x: 1,
        ease: Power4.easeInOut
      }, delay)
      .fromTo(mask.position, 1, {
        x: -width / 2
      }, {
        x: '+=' + width / 2,
        ease: Power4.easeInOut
      }, delay)
    group.add(mask)
  }

  this.scene.add(group)
  return {
    group: group,
    timeline: timeline
  }
}
Canvas.prototype.makeMask = function (align = 'left') {
  let sign = align === 'left' ? -1 : 1
  let group = new THREE.Group()
  group.name = 'mask'
  let timeline = new TimelineMax({ paused: true })
  // let pattern = [
  //   [470, 400],
  //   [150, 390],
  //   [580, 540],
  //   [180, 300],
  // ]
  let w = window.innerWidth
  let h = window.innerHeight

  for (var j = 0; j < 4; j++) {
    let height = h / 4
    let width = w * getRandom(0.28, 0.48)
    let offset = j % 2 ? ((width / 2.2) * sign) : (width * 1.2 * sign)
    // let width = pattern[j][1]
    // let offset = pattern[j][0] * sign
    if (j === 3) {
      width = w * 0.35
      offset = width * 0.2 * sign
    }
    if (j === 1 || j === 2) {
      width = w * 0.5
      offset = j % 2 ? ((width / 4.2) * sign) : (width * sign)
    }

    let direction = j % 2 ? '+=' : '-='
    let geo = new THREE.PlaneGeometry(width, height, 2, 1)
    let mat = new THREE.MeshBasicMaterial({ color: 0x000000, opacity: 0 })
    let mask = new THREE.Mesh(geo, mat)

    let delay = Math.random() * 0.5

    mask.position.set(offset, height * j - h / 2 + height / 2, 0)
    mask.scale.x = 0.000001

    timeline
      .to(mask.scale, 1, {
        x: 1,
        ease: Power4.easeInOut
      }, delay)
      .to(mask.position, 1, {
        x: direction + width / 2 * sign,
        ease: Power4.easeInOut
      }, delay)

    group.add(mask)
  }

  this.scene.add(group)
  return {
    group: group,
    timeline: timeline
  }
}

Canvas.prototype.makeLines = function () {
  let that = this
  let group = new THREE.Group()
  group.name = 'motion'
  let timeline = new TimelineMax({
    paused: true,
    onComplete: function () {
      this.pause()
      this.time(0)
      that.scene.remove(group)
      that.makeLines()
    }
  })

  for (let i = 0; i < 20; i++) {
    let geo = new THREE.PlaneGeometry(1, 1)
    let mat = new THREE.MeshBasicMaterial({ color: 0x2A3572 })
    let line = new THREE.Mesh(geo, mat)
    let scaleX = 50 * Math.random()
    let scaleY = window.innerHeight / 3
    let sign = Math.cos(Math.PI * Math.round(Math.random()))
    line.material.color.multiplyScalar(getRandom(1, i))
    line.scale.set(scaleX, scaleY, 1)
    line.position.set(
      sign * getRandom(window.innerWidth / 3, window.innerWidth),
      -window.innerHeight / 2 - scaleY / 2,
      -0.5
    )

    group.add(line)
    timeline.to(line.position, getRandom(1, 2), {
      y: window.innerHeight,
      ease: Expo.easeInOut,
      delay: Math.random()
    }, 0)
  }
  this.motionSwitch = timeline
  this.scene.add(group)
}

Canvas.prototype.makeOverlay = function (color) {
  let geo = new THREE.PlaneGeometry(window.innerWidth, window.innerHeight, 1, 1)
  let mat = new THREE.MeshBasicMaterial({ color: color, opacity: 0, transparent: true })
  let cover = new THREE.Mesh(geo, mat)
  cover.name = 'background'
  cover.position.set(0, 0, -1)

  this.sceneHUD.add(cover)
  return cover
}

Canvas.prototype.switch = function () {
  this.motionSwitch.play()
}

Canvas.prototype.resize = function resize () {
  let aspect = window.innerWidth / window.innerHeight

  this.camera.aspect = aspect
  this.camera.updateProjectionMatrix()

  this.cameraHUD.aspect = aspect
  this.cameraHUD.updateProjectionMatrix()

  this.renderer.setSize(window.innerWidth, window.innerHeight)
}

Canvas.prototype.init = function init (container) {
  this.container = container
  this.container.appendChild(this.renderer.domElement)

  window.addEventListener('resize', () => this.resize())

  this.animate()
}

Canvas.prototype.animate = function animate () {
  requestAnimationFrame(() => this.animate())
  this.render()
}

Canvas.prototype.render = function render () {
  this.renderer.render(this.scene, this.camera)
  this.renderer.render(this.sceneHUD, this.cameraHUD)
}

function getRandom (min, max) {
  return Math.random() * (max - min) + min
}
